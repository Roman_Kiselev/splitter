package ru.splitter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Splitter<T> {
    private List<T> passed;
    private List<T> notPassed;

    private Splitter(List<T> passed, List<T> notPassed){
        this.passed = passed;
        this.notPassed = notPassed;
    }

    public static <T>Splitter<T> splitBy(Collection<T> collection, Predicate<T> test){
        List<T> passed = new ArrayList<>();
        List<T> notPassed = new ArrayList<>();

        collection.forEach(item ->{
            if (test.test(item)){
                passed.add(item);
            }else {
                notPassed.add(item);
            }
        });
        return new Splitter<>(passed, notPassed);
    }

    public Splitter<T> workWithPassed(Consumer<Stream<T>> func){
        func.accept(passed.stream());
        return this;
    }

    public Splitter<T> workWithNotPassed(Consumer<Stream<T>> func){
        func.accept(notPassed.stream());
        return this;
    }
}
