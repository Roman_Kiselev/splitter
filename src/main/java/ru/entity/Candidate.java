package ru.entity;

public class Candidate {
    private boolean won;
    private String name;

    public Candidate(boolean won, String name) {
        this.won = won;
        this.name = name;
    }

    public boolean isWon() {
        return won;
    }

    public String getName() {
        return name;
    }
}
