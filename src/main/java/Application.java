import ru.entity.Candidate;
import ru.splitter.Splitter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Application {
    public static void main(String[] arg){
        workOnNumbers();
        sendEmail();
    }

    private static void workOnNumbers(){
        List<Integer> numbers = IntStream.range(1, 11).boxed().collect(Collectors.toList());
        Splitter.splitBy(numbers, num -> num%2 == 0)
                .workWithPassed(passed ->
                        passed.forEach(even -> System.out.println("" + even + " -> " + even)))
                .workWithNotPassed(notPassed ->
                        notPassed.forEach(odd -> System.out.println("" + odd + " -> " + (odd * odd))
                                ));
    }

    private static void sendEmail(){
        List<Candidate>candidates = getCandidates();
        Splitter.splitBy(candidates, Candidate::isWon)
                .workWithPassed(winners ->
                    winners.forEach(winner -> System.out.println("Dear, " + winner.getName() + ", you won!")))
                .workWithNotPassed(notWinners ->
                    notWinners.forEach(notWinner -> System.out.println("Dear, " + notWinner.getName() + ", you lost, sorry!")));
    }

    private static List<Candidate> getCandidates(){
        Random random = new Random();
        return new ArrayList<Candidate>(){{
            add(new Candidate(random.nextBoolean(), "Ivan"));
            add(new Candidate(random.nextBoolean(), "Mary"));
            add(new Candidate(random.nextBoolean(), "Dan"));
            add(new Candidate(random.nextBoolean(), "Margo"));
            add(new Candidate(random.nextBoolean(), "Marco"));
            add(new Candidate(random.nextBoolean(), "Polo"));
            add(new Candidate(random.nextBoolean(), "Danny"));
        }};
    }
}
